// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, non_constant_identifier_names, prefer_typing_uninitialized_variables, must_be_immutable, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'chat.dart';
import 'titlebar.dart';
import 'inputbar.dart';

var mock_data_text = "        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.";

var mock_data_text_longer = "        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.";

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: CustomAppBar(),

      backgroundColor: Colors.grey,
      body: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        // MOCK DATA TEMPORÁRIO
        // CLASSE = ChatData

        // Converter para listView


        // TODO: Arrumar ListView de mensagens enviadas.
        // Alternativa que pode ser usada: SingleChildScrollView
        // url: https://stackoverflow.com/questions/62783181/flutter-how-to-scroll-a-container-rather-than-its-content


        // ListView(
        //   padding: const EdgeInsets.all(8),
        //   children: <Widget>[
        //     Container(
        //       height: 50,
        //       color: Colors.amber[600],
        //       child: const Center(child: Text('Entry A')),
        //     ),
        //     Container(
        //       height: 50,
        //       color: Colors.amber[500],
        //       child: const Center(child: Text('Entry B')),
        //     ),
        //     Container(
        //       height: 50,
        //       color: Colors.amber[100],
        //       child: const Center(child: Text('Entry C')),
        //     ),
        //  ],
        // ),

        ChatBubble(local_user: false, data: ChatData(text: mock_data_text, sender: "Usuário 1", user: false)),
        ChatBubble(local_user: true, data: ChatData(text: mock_data_text_longer, sender: "Usuário 2", user: false)),
        ChatBubble(local_user: false, data: ChatData(text: mock_data_text, sender: "Usuário 1", user: false)),
        ChatBubble(local_user: true, data: ChatData(text: mock_data_text, sender: "Usuário 2", user: false)),
        ChatBubble(local_user: false, data: ChatData(text: mock_data_text, sender: "Usuário 1", user: false)),
        ChatBubble(local_user: true, data: ChatData(text: mock_data_text, sender: "Usuário 2", user: false)),

        // TODO: ARRUMAR ERRO DA JANELA DE ENTRADA
        // Arrumado parcialmente, precisa arrumar o flexbox temporário.
        // Criar um container que vai receber as janelas de mensagem, ao invés de deixar as mensagens soltas pela tela.
        
        Spacer(),
        
        InputBar(),

        // Barra de entrada
        
      ]),
    ),
  ));
}